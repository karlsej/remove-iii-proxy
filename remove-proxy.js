$(document).ready(function(){
    $(".s-lg-book-cover img").each(function() {  // some people are putting proxied cover links in, so this removes proxy from those
        var a = $(this);
        a.src = a.src.replace('http://0-', 'http://'); // remove the first part
        a.src = a.src.replace('.lasiii.losrios.edu', ''); // remove the last part
});
