# Remove iii proxy from book images in Libguides
In Libguides, when book images aren't pulled automatically, we can get them from EDS. But if you've gone into proxied EDS, the image source will be proxied, and won't work off-campus until users authenticate. So the script removes the proxy from this content only (not the links, which need the proxy).
